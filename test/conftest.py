import pytest
import requests
from datetime import datetime
from pytest_metadata.plugin import metadata_key

# Initialize counters
passed_count = 0
failed_count = 0
skipped_count = 0
total_count = 0


def pytest_configure(config):
    config.stash[metadata_key]["Test Executed By"] = "Deekshith"


def pytest_html_report_title(report):
    report.title = "Test-Report"


def pytest_html_results_summary(prefix, summary, postfix):
    prefix.extend(["<p>Suite Description: API testing of IDOCX API's</p>"])


def pytest_html_results_table_header(cells):
    cells.insert(2, "<th>Description</th>")


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    global passed_count, failed_count, skipped_count, total_count
    outcome = yield
    report = outcome.get_result()
    report.description = item.function.__doc__

    if call.when == 'call':
        total_count += 1
        if call.excinfo is None:
            passed_count += 1
        elif call.excinfo.errisinstance(pytest.skip.Exception):
            skipped_count += 1
        else:
            failed_count += 1


def pytest_html_results_table_row(report, cells):
    if hasattr(report, "description"):
        cells.insert(2, f"<td>{report.description}</td>")
    else:
        cells.insert(2, "<td>No description available</td>")


def pytest_addoption(parser):
    parser.addoption("--script-name", action="store", default="")


@pytest.fixture(scope='session', autouse=True)
def script_name_storage(request):
    script_name = request.config.getoption("--script-name")
    return {'script_name': script_name}


@pytest.fixture(scope='session', autouse=True)
def final_cleanup(request, script_name_storage):
    yield
    current_date_time = datetime.now().strftime("%m-%d-%Y %I:%M:%S %p")
    script_name = request.config.getoption("--script-name")
    message = (f'[b]Regression Testing Summary for IDOCX ({script_name})[/b]\nDate: {current_date_time}\n\n'
               f'Passed: {passed_count}\nFailed: {failed_count}\nASkipped: {skipped_count}\n'
               f'Total Test Cases: {total_count}')
    response = requests.get(
        'https://sirmaglobal.bitrix24.com/rest/118/b2iecuj66krfw9kk/imbot.message.add.json',
        params={
            'BOT_ID': '214',
            'CLIENT_ID': '6gvjn227c5aayn2qo6ur6e16rpbwtn4r',
            'DIALOG_ID': '216',
            'MESSAGE': message
        }
    )

    if response.status_code == 200:
        print("Bitrix Notification Sent!")
    else:
        print(f"Failed to send Bitrix notification: {response.status_code}, {response.text}")


# Adding this to ensure the counts are reset before each session
def pytest_sessionstart(session):
    global passed_count, failed_count, skipped_count
    passed_count = 0
    failed_count = 0
    skipped_count = 0
