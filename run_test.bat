@echo off
REM Navigate to the project directory
cd "C:\vscode\Git Lab\idcox-api-testscript\test"

REM Activate the Python environment
call D:\IBorgAgentSetup2.1.6\iborgenv\Scripts\activate

REM Run the pytest test script
pytest --script-name="Manual Workspace"	-v manualworkspace_api_test.py
pytest --script-name="Auto Workspace" -v autoworkspace_api_test.py   
pytest --script-name="Member Workspace"	-v memberworkspace_api_test.py

REM Keep the Command Prompt window open
echo The test script has finished running.
pause

REM Deactivate the Python environment
call D:\IBorgAgentSetup2.1.6\iborgenv\Scripts\deactivate

@echo on
